const Tokenizer = require("node-vntokenizer");
const token = new Tokenizer();
const products = require("./data/products.json");
const keywords = require("./data/keywords");

const { KMPSearch, removeAccents, KMPSearchAdvance } = require("./functions");

const fs = require("fs");

function search() {
  const begin = Date.now();
  const limit = 10;
  const finalResults = {
    full_name: "Trương Viết Huy Phong",
    result: {},
  };
  keywords.map((key) => {
    const formatKey = key
      .trim()
      .toLowerCase()
      .replace(/[\u2000-\u200f]/g, "");
    const keyNotAccent = removeAccents(formatKey).toLowerCase();
    //tokenize
    const keyTokenize = token.tokenize(formatKey);
    const keyNotAccentTokenize = token.tokenize(keyNotAccent);
    //result data;
    let results = [];
    let resultsIndex = [];
    let resultsNotAccent = [];
    let resultsNotAccentIndex = [];
    let resultsTokenize = [];
    let resultsTokenizeIndex = [];
    //search

    //search có dấu full text
    products.map((product, index) => {
      const fullTextSearch = KMPSearch(
        formatKey,
        product.name.trim().toLowerCase()
      );
      if (fullTextSearch !== -1) {
        results.push({ ...product, score: -1 });
        resultsIndex.push(index);
      }
    });

    //search ko dấu full text
    // products.map((product, index) => {
    //   const fullTextSearch = KMPSearch(
    //     formatKey,
    //     product.name.trim().toLowerCase()
    //   );
    //   if (fullTextSearch !== -1) {
    //     results.push({ ...product, score: -1 });
    //     resultsIndex.push(index);
    //   }
    // });

    //search token co dau
    // products.map((product, index) => {
    //   keyTokenize.map((value) => {
    //     const fullTextSearch = KMPSearch(
    //       key,
    //       product.name.trim().toLowerCase()
    //     );
    //     if (fullTextSearch !== -1) {
    //       resultsTokenize.push({ ...product, score: 0 });
    //       resultsTokenizeIndex.push(index);
    //     }
    //   });
    // });

    //search không dấu tokenize
    products.map((product, index) => {
      const productNameRemoveAccent = removeAccents(
        product.name.trim().toLowerCase()
      );
      keyNotAccentTokenize.map((value, tokenIndex) => {
        const tokenizeResult = KMPSearchAdvance(value, productNameRemoveAccent);
        if (tokenizeResult.length > 0) {
          //Nếu fulltext chưa có thì thêm vào kết qủa của not accent
          if (
            resultsIndex.indexOf(index) === -1
            //|| resultsTokenizeIndex.indexOf(index) === -1
          ) {
            const tokenizeResultIndex = resultsNotAccentIndex.indexOf(index);

            if (tokenizeResultIndex === -1) {
              resultsNotAccentIndex.push(index);
              resultsNotAccent.push({
                ...product,
                score:
                  tokenIndex === 0
                    ? 0.1 +
                      tokenizeResult.length / productNameRemoveAccent.length
                    : tokenizeResult.length / productNameRemoveAccent.length,
              });
            }
            //từ tiếp theo trong chuỗi keyword tiếp tục nằm trong product names - cộng thêm score cho kết quả tìm được
            else {
              resultsNotAccent[tokenizeResultIndex].score +=
                tokenizeResult.length / productNameRemoveAccent.length;
            }
          }
        }
      });
    });
    resultsNotAccent.sort((a, b) => (a.score > b.score ? -1 : 1));
    finalResults.result[key] = [
      ...results,
      ...resultsTokenize,
      ...resultsNotAccent,
    ].slice(0, limit);
  });
  fs.writeFile("data/output.json", JSON.stringify(finalResults), (error) => {
    if (error) throw error;
    console.log("time: ", Date.now() - begin, "ms");
  });
}

search();

// console.log(
//   KMPSearchAdvance("điện", "điện thoại gọi điện cả ngày không hết pin")
// );
