function KMPSearch(pat, txt) {
  const M = pat.length;
  const N = txt.length;

  let lps = Array(M).fill(0);
  let j = 0;
  let i = 0;
  computeLPSArray(pat, M, lps);
  while (i < N) {
    if (pat[j] === txt[i]) {
      i += 1;
      j += 1;
    }

    if (j == M) {
      return i - j;
    } else if (i < N && pat[j] !== txt[i])
      if (j !== 0) {
        j = lps[j - 1];
      } else i += 1;
  }
  return -1;
}

function KMPSearchAdvance(pat, txt) {
  const M = pat.length;
  const N = txt.length;

  let lps = Array(M).fill(0);
  let j = 0;
  let i = 0;
  computeLPSArray(pat, M, lps);
  const results = [];
  while (i < N) {
    if (pat[j] === txt[i]) {
      i += 1;
      j += 1;
    }

    if (j == M) {
      results.push(i - j);
      j = lps[j - 1];
    } else if (i < N && pat[j] !== txt[i])
      if (j !== 0) {
        j = lps[j - 1];
      } else i += 1;
  }
  return results;
}

function computeLPSArray(pat, M, lps) {
  let len = 0;

  let i = 1;
  while (i < M) {
    if (pat[i] == pat[len]) {
      len += 1;
      lps[i] = len;
      i += 1;
    } else {
      if (len !== 0) {
        len = lps[len - 1];
      } else {
        lps[i] = 0;
        i += 1;
      }
    }
  }
}

function search({ isRemoveAccent = false, fuzzySearch = false }) {
  if (isRemoveAccent) {
    const productNameRemoveAccent = removeAccents(
      product.name.trim().toLowerCase()
    );
    const fullTextResult = KMPSearch(keyNotAccent, productNameRemoveAccent);
    if (fullTextResult !== -1) {
      resultsIndex.push(index);
      results.push({ ...product, score: 1 });
    }
    if (fuzzySearch) {
      keyNotAccentTokenize.map((value) => {
        const tokenizeResult = KMPSearch(value, productNameRemoveAccent);
        if (tokenizeResult !== -1 && resultsIndex.indexOf(index) === -1) {
          resultsIndex.push(tokenizeResult);
          results.push({
            ...product,
            score:
              1 +
              stringSimilarity.compareTwoStrings(
                value,
                productNameRemoveAccent
              ),
          });
        }
      });
    }
  } else {
    const fullTextResult = KMPSearch(key, product.name.trim().toLowerCase());
    if (fullTextResult !== -1) {
      resultsIndex.push(index);
      results.push({ ...product, score: 1 });
    }
    if (fuzzySearch) {
      keyTokenize.map((value) => {
        const tokenizeResult = KMPSearch(
          value,
          product.name.trim().toLowerCase()
        );
        if (tokenizeResult !== -1 && resultsIndex.indexOf(index) === -1) {
          resultsIndex.push(tokenizeResult);
          results.push({
            ...product,
            score:
              1 +
              stringSimilarity.compareTwoStrings(value, product.name.trim()),
          });
        }
      });
    }
  }
}

function removeAccents(str) {
  //str = str.replace(/\s+/g, ' ');
  str = str.trim();
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
  str = str.replace(/Đ/g, "D");
  return str;
}

module.exports = {
  KMPSearch,
  KMPSearchAdvance,
  removeAccents,
};
